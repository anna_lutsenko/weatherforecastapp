# Weather Provider
## Architecture 
Application based on standard MVC Architecture.
## About design
I've tried implement design which was provided in mockups.
There's singl screen with collection view.
![main](https://bitbucket.org/anna_lutsenko/weatherforecastapp/raw/02dbdcfe409273b3ef526b094cbbd55ff8682f3e/images/2.png)
## API Classes
There are 2 classes which is used for working with API:
* `APIClient`: this client is used for making all HTTP requests
* `WeatherMarager`: this is high level class for providing weather from provided resourse. It can be created with API client which is injected on initialization.
## Error handlers
Application notifies user that something went wrong. E.g. if there's no internet connection user will be view with message and `Retry` button.
![error_handler](https://bitbucket.org/anna_lutsenko/weatherforecastapp/raw/02dbdcfe409273b3ef526b094cbbd55ff8682f3e/images/1.png)
## Known issues and features to improve / implement
* UI doesn't look good on small screens (4-inch)
* Currently Trondheim's coordinates are hardcoded. It should be better to add Location service which gets user's coordinates and fetch weather based on them.
* There's no loader view