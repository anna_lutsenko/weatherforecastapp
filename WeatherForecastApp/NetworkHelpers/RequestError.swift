//
//  RequestError.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/17/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

enum RequestError: Error {
    case noInternetConnection
    case wrongURL
    case wrongData
    case unknownError
    
    var localizedDescription: String {
        switch self {
        case .noInternetConnection:
            return "Please check your internet connection and try again."
        case .wrongURL:
            return "URL is not correct"
        case .wrongData:
            return "Server returned wrong data. Try later please"
        default:
            return "Ops! Something went wrong. Could you try later."
        }
    }
}
