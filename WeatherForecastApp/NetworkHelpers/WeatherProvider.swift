//
//  WeatherProvider.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/17/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import CoreLocation

private struct CoordinatesParams: Encodable {
    let coordinates: CLLocationCoordinate2D
    
    private enum CodingKeys: String, CodingKey {
        case lat, lon
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinates.latitude, forKey: .lat)
        try container.encode(coordinates.longitude, forKey: .lon)
    }
}

/// This class serves for providing actual weather.
class WeatherManager {
    let apiClient: APIClient
    
    /// Create WeatherManager with api client.
    ///
    /// - Parameter apiClient: API Client. By default it's a standard API Client which returns weather from server. It can be replaced with mock.
    init(apiClient: APIClient = APIClient()) {
        self.apiClient = apiClient
    }
    
    /// Returns current weather based on coordinates.
    func getWeather(coordinates: CLLocationCoordinate2D, completion: @escaping (Result<[WeatherModel], Error>) -> Void) {
        let params = CoordinatesParams(coordinates: coordinates)
        self.apiClient.run(url: APIConstants.baseURL, httpMethod: .post, param: params, completion: completion)
    }
}
