//
//  NetworkManager.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/17/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import UIKit

enum HttpMethod: String {
    case get, connect, post, patch, put, delete
    var name: String {
        return self.rawValue.uppercased()
    }
}

/// APIClient is a helper for HTTP Requests
class APIClient {
    
    /// Generic method for makirg all king of requests
    ///
    /// - Parameters:
    ///   - url: Request URL
    ///   - httpMethod: HTTP Method
    ///   - param: parameters
    ///   - completion: Completion handler
    func run<Param: Encodable, TypeResult: Decodable>(url: String,
                                                      httpMethod: HttpMethod = .get,
                                                      param: Param? = nil,
                                                      completion: @escaping (Result<TypeResult, Error>) -> Void) {
        let request: URLRequest
        do {
            switch httpMethod {
            case .get, .connect:
                request = try noBody(for: httpMethod, url: url, params: param)
            default:
                request = try bodyRequest(for: httpMethod, url: url, params: param)
            }
        } catch let error {
            completion(.failure(error))
            return
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { data, response, error in
            if let nsError = error as NSError? {
                let errorCode = nsError.code
                /// -1009 is the OFFLINE error code
                /// HTTP load failed (error code: -1009)
                if errorCode == -1009 {
                    completion(.failure(RequestError.noInternetConnection))
                }
            }
            
            if let error = error {
                Log(category: .network, type: .error).log(message: "Request failed with error:  \(error)")
                completion(.failure(error))
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                do {
                    let weather = try JSONDecoder().decode(TypeResult.self, from: data)
                    completion(.success(weather))
                } catch let error {
                    Log(category: .network, type: .error).log(message: "Cannot decode model type \(TypeResult.self)")
                    completion(.failure(error))
                }
            } else {
                Log(category: .network, type: .error).log(message: "Request failed")
                completion(.failure(RequestError.unknownError))
            }
        }
        task.resume()
    }
}

extension APIClient {
    
    /// This method is used for such HTTP requests as GET, CONNECT, HEAD, etc.
    /// These requests don't have requests body and parameters are added as URL Query parameters
    private func noBody<E: Encodable>(for method: HttpMethod, url: String, params: E) throws -> URLRequest {
        
        let request = try self.genericRequest(url: url, method: method)
        // TODO: add params to URL query
        return request
    }
    
    /// This method is user for creating POST, PATCH, UPDATE, etc.
    /// Params passed to this method will be Encoded and added to request as HTTP Body
    private func bodyRequest<E: Encodable>(for method: HttpMethod,
                                           url: String,
                                           params: E) throws -> URLRequest {
        var request = try self.genericRequest(url: url, method: method)
        let body = try JSONEncoder().encode(params)
        request.httpBody = body
        return request
    }
    
    /// That is method for creating generic requests.
    /// It adds URL, default headers and method to request
    private func genericRequest(url: String, method: HttpMethod) throws -> URLRequest {
        guard let url = URL(string: url) else {
            throw RequestError.wrongURL
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.name
        request.allHTTPHeaderFields = ["x-api-key":"pJHFbhCxY29tGBpQK1Zlf4NMdkdkKhFI6iYVymeJ"]
        
        return request
    }
}

class MockAPIClient: APIClient {
    override func run<Param, TypeResult>(url: String, httpMethod: HttpMethod = .get, param: Param? = nil, completion: @escaping (Result<TypeResult, Error>) -> Void) where Param : Encodable, TypeResult : Decodable {
        
        guard let asset = NSDataAsset(name: "weather") else {
            fatalError("Missing data asset: weather")
        }
        
        do {
            let weather = try JSONDecoder().decode(TypeResult.self,
                                                   from: asset.data)
            completion(.success(weather))
        } catch let error {
            completion(.failure(error))
        }
    }
}

