//
//  UICollectionView.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/18/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit

extension UICollectionView {
    func dequeueCell<T: UICollectionViewCell>(ofType type: T.Type, indexPath: IndexPath) -> T {
        let cellId = String(describing: type)
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? T else {
            fatalError("Can not dequeue cell")
        }
        return cell
    }
}
