//
//  DateFormatter.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/18/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let baseDF: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }()
    
    static let shortDF: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        return formatter
    }()
}
