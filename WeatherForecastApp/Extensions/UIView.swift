//
//  UIView.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/19/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self),
                                        owner: nil,
                                        options: nil)![0] as! T
    }
}
