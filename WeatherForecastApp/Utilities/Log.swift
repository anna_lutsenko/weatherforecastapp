//
//  Log.swift
//  WeatherForecastApp
//
//  Created by Anna on 9/1/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import os.log

struct Log {
    let category: LogCategory
    let type: OSLogType
    
    init(category: LogCategory, type: OSLogType?) {
        self.category = category
        self.type = type ?? OSLogType.default
    }
    
    func log(message: String) {
        os_log("%@", log: self.category.osLog(), type: self.type, message)
    }
}

struct LogCategory: Equatable, RawRepresentable {
    let rawValue: String
    
    init?(rawValue: String) {
        self.rawValue = rawValue
    }
    
    func osLog() -> OSLog {
        return OSLog(subsystem: Bundle.main.bundleIdentifier!, category: self.rawValue)
    }
}

extension LogCategory {
    static let ui = LogCategory(rawValue: "ui")!
    static let network = LogCategory(rawValue: "network")!
}
