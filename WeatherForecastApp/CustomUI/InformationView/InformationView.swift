//
//  InformationView.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/18/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit

class InformationView: UIView {
    @IBOutlet private weak var informationLabel: UILabel!
    @IBOutlet private weak var actionButton: UIButton! {
        didSet {
            actionButton.layer.borderWidth = 1
            actionButton.layer.borderColor = UIColor(named: "SearisGreen")?.cgColor
        }
    }
    private var viewModel: InformationViewModel?
    
    func setup(with viewModel: InformationViewModel) {
        self.viewModel = viewModel
        informationLabel.text = viewModel.info
        actionButton.setTitle(viewModel.titleButton, for: .normal)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        viewModel?.completion?()
    }
}
