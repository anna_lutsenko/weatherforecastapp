//
//  InformationViewModel.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/19/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

struct InformationViewModel {
    let info: String
    let titleButton: String
    let completion: VoidCallback?
}

extension InformationViewModel {
    static func noInternet(_ completion: VoidCallback?) -> InformationViewModel {
        return InformationViewModel(info: Constants.noInternet,
                                    titleButton: Constants.Titles.retry,
                                    completion: completion)
    }
    
    static func tryAgain(_ completion: VoidCallback?) -> InformationViewModel {
        return InformationViewModel(info: Constants.tryAgain,
                                    titleButton: Constants.Titles.retry,
                                    completion: completion)
    }
}
