//
//  WeatherModel.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/17/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation
import UIKit

struct WeatherModel: Codable {
    let date: String
    let temperature: Double
    let precipitation: Double
    let symbol: Int
    let text: String
    
    enum CodingKeys: String, CodingKey {
        case date = "Date"
        case temperature = "Temperature"
        case precipitation = "Precipitation"
        case symbol = "WeatherSymbol"
        case text = "WeatherText"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        date = try container.decode(String.self, forKey: .date)
        temperature = try container.decode(Double.self, forKey: .temperature)
        precipitation = try container.decode(Double.self, forKey: .precipitation)
        symbol = try container.decode(Int.self, forKey: .symbol)
        text = try container.decode(String.self, forKey: .text)
    }
}

/// WeatherViewModel is created based on WeatherModel.
/// It provides readable data for user.
struct WeatherViewModel {
    let image: UIImage?
    let temperature: String
    let precipitation: String
    let date: String?
    
    init(model: WeatherModel) {
        self.image = UIImage(named: String(model.symbol))
        
        let temperatureMeasurement = Measurement(value: model.temperature,
                                                 unit: UnitTemperature.celsius)
        let formatter = MeasurementFormatter()
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 0
        formatter.numberFormatter = numberFormatter
        self.temperature = formatter.string(from: temperatureMeasurement)
        
        self.precipitation = "\(Int(model.precipitation))%"
        
        self.date = DateFormatter.baseDF.date(from: model.date).map {
            Calendar.current.isDateInToday($0) ? "Today" : DateFormatter.shortDF.string(from: $0)
        }
    }
}
