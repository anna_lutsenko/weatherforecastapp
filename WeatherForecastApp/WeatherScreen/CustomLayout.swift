//
//  CustomLayout.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/19/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit

class CustomLayout: UICollectionViewFlowLayout {
    struct CellPrefs {
        static let padding: CGFloat = 48
        static let cellSpacing: CGFloat = 32
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let cv = self.collectionView else { return .zero }
        
        let cellWidth = cv.frame.size.width - CellPrefs.padding * 2 + CellPrefs.cellSpacing
        let itemIndex = round(proposedContentOffset.x / cellWidth)
        let xOffset = itemIndex * cellWidth
        return CGPoint(x: xOffset, y: 0)
    }
}
