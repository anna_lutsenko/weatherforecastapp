//
//  WeatherViewController.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/17/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit
import CoreLocation
import os.log

class WeatherViewController: UIViewController {
    // MARK: - UI
    @IBOutlet private weak var collectionView: UICollectionView!
    private lazy var infoView: InformationView = {
        let infoView: InformationView = .fromNib()
        return infoView
    }()
    
    // MARK: Data
    private let weatherManager = WeatherManager(apiClient: APIClient())
    private var weatherModels: [WeatherModel] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.decelerationRate = .fast
        getWeather()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(getWeather),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    // MARK: Private methods
    
    @objc private func getWeather() {
        let trondheimCoord = CLLocationCoordinate2D(latitude: 63.446827,
                                                    longitude: 10.421906)
        weatherManager.getWeather(coordinates: trondheimCoord) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let weather):
                    self?.infoView.removeFromSuperview()
                    self?.weatherModels = weather
                case .failure(let error):
                    let completion: VoidCallback = { [weak self] in
                        self?.infoView.removeFromSuperview()
                        self?.getWeather()
                    }
                    var infoViewModel = InformationViewModel.tryAgain(completion)
                    
                    if let requestError = error as? RequestError,
                        requestError == .noInternetConnection {
                        infoViewModel = InformationViewModel.noInternet(completion)
                    }
                    self?.displayInfoView(with: infoViewModel)
                }
            }
        }
    }
    
    private func displayInfoView(with viewModel: InformationViewModel) {
        infoView.setup(with: viewModel)
        infoView.frame = view.bounds
        view.addSubview(infoView)
    }
}

extension WeatherViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(ofType: WeatherCollectionViewCell.self, indexPath: indexPath)
        let viewModel = WeatherViewModel(model: weatherModels[indexPath.row])
        cell.setup(with: viewModel)
        return cell
    }
}

extension WeatherViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cvSize = collectionView.frame.size
        cvSize.width -= CustomLayout.CellPrefs.padding * 2
        cvSize.height -= CustomLayout.CellPrefs.padding * 2
        return cvSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CustomLayout.CellPrefs.cellSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: CustomLayout.CellPrefs.padding, bottom: 0, right: CustomLayout.CellPrefs.padding)
    }
}
