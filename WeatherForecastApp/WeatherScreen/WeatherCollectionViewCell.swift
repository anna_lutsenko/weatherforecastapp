//
//  WeatherCollectionViewCell.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/18/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import UIKit

class WeatherCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var precipitationLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        self.contentView.layer.cornerRadius = 16.0
        self.contentView.layer.masksToBounds = true
        
        self.layer.cornerRadius = 16
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 8.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
    }
    
    func setup(with viewModel: WeatherViewModel) {
        imageView.image = viewModel.image
        dateLabel.text = viewModel.date
        temperatureLabel.text = viewModel.temperature
        precipitationLabel.text = viewModel.precipitation
    }
}
