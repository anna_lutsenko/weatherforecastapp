//
//  Constants.swift
//  WeatherForecastApp
//
//  Created by Anna on 8/18/19.
//  Copyright © 2019 Anna. All rights reserved.
//

import Foundation

// MARK: - typealias
typealias VoidCallback = ()->Void

struct Constants {
    static let noInternet = "Turn on mobile data or use \nWi-Fi to access the \nweather forecast"
    static let tryAgain = "Something whent wrong try again, please"
    
    struct Titles {
        static let retry = "Retry"
    }
}
